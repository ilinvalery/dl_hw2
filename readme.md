# DL - HW2 Valery Ilin
```
python3 -m venv venv
source venv/bin/activate
pip install -r req.txt
jupyter lab
```
Copy *tiny-imagenet-2020.zip* to the folder

Then open *Part_2_Valery_Ilin_fixed.ipynb* (no changes, the same version that I sent to you earlier) and launch all cells